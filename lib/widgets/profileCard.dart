import 'package:flutter/material.dart';
import 'package:flutter_example/widgets/profileDetails.dart';

const String short_name = "KH";
const String full_name = "Kakashi Hatake";
const String programme = "Sixth Hokage";

class ProfileCard extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Expanded(
              flex: 50,
              child: GestureDetector(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context){
                    return ProfileDetails();
                  }));
                },
                child: Card(
                  elevation: 10,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex: 30,
                        child: Container(
                          margin: EdgeInsets.all(5),
                          child: CircleAvatar(
                            radius: 32,
                            child: Text("$short_name"),
                          ),
                        ),
                      ),
                      Expanded(
                          flex: 70,
                          child: Container(
                            margin: EdgeInsets.all(10),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "$full_name",
                                  textAlign: TextAlign.center,
                                ),
                                Divider(),
                                Text("$programme"),
                              ],
                            ),
                          )
                      ),
                    ],
                  ),
                ),
              )
          )
        ],
      ),
    );
  }
}