import 'package:flutter/material.dart';

const String short_name = "FiT3";
const String full_name = "Muhammad Nurfitri Bin Muhamad Sazali";
const String programme = "CS230";

Widget MyLists(){
  return SizedBox(
    child: ListView(
      children: <Widget>[
        _MyLists(),
        _MyLists(),
        _MyLists(),
        _MyLists(),
      ],
    ),
  );
}

Widget _MyLists(){
  return Card(
    elevation: 10,
    child: Container(
      margin: EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 20,
            child: Container(
              height: 120.0,
              width: 120.0,
              child: Center(
                child: RichText(
                  text: TextSpan(
                      text: "<Text>",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                      )
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );
}
