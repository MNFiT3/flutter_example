import 'package:flutter/material.dart';

//Data for page 1
var s1 = [
  _details('Full Name', 'Kakashi Hatake'),
  _details('Date of birth', 'September 15'),
  _details('Hometown', 'Konoha'),
  _details('Job', 'Hokage'),
  _details('Hobby', 'Reading Novel'),
];

//Data for page 2
var s2 = [
  _details('Achievement 1', 'bla2'),
  _details('Achievement 2', 'bla2'),
  _details('Achievement 3', 'bla2'),
  _details('Achievement 4', 'bla2'),
  _details('Achievement 5', 'bla2'),
  _details('Achievement 6', 'bla2'),
];



class ProfileDetails extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        child: _carouselBuilder(context),
      ),
    );
  }
}

//Build carousel
Widget _carouselBuilder(BuildContext context) {
  return Center(
    child: Column(
      children: <Widget>[
        Expanded(
          flex: 100,
          child: SizedBox(
            child: PageView(
              children: <Widget>[
                _profileDetails(context, "Biography", s1),
                _profileDetails(context, "Achievement", s2),
              ],
            ),
          ),
        )
      ],
    ),
  );
}

//Create styled profile details
Widget _profileDetails(BuildContext context, String title, var arrDatas) {
  return Container(
    child: Stack(
      children: <Widget>[
        Container(
          color: Colors.grey.shade400,
          height: MediaQuery.of(context).size.height,
          width: 65,
          child: Center(
              child: RotatedBox(
                  quarterTurns: -1,
                  child: Text(
                    title,
                    style: TextStyle(
                      letterSpacing: 7,
                      fontStyle: FontStyle.italic,
                    ),
                  ))),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(65.0, 50.0, 0.0, 25.0),
          child: Column(
            children: arrDatas,
          ),
        )
      ],
    )
  );
}

//Create formatted title and the data
Widget _details(String title, String data) {
  return Container(
    child: Column(
      children: <Widget>[
        Text(title, style: TextStyle(fontSize: 25),),
        Container(height: 10,),
        Text(data, style: TextStyle(fontSize: 15),),
        Container(height: 10,),
        Divider(),
      ],
    ),
  );
}
