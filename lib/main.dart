import 'package:flutter/material.dart';
import 'package:flutter_example/widgets/MyLists.dart';

import 'widgets/profileCard.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Flutter Example",
        home: SafeArea(child: PageOne()),
    );
  }
}

class PageOne extends StatefulWidget{
  _PageOne createState() => new _PageOne();
}

class _PageOne extends State<PageOne>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueAccent,
      appBar: AppBar(
        title: Text("My Info"),
      ),
      body: Container(
        margin: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 30,
              child: ProfileCard(),
            ),
            Expanded(
              flex: 11,
              child: Column(
                children: <Widget>[
                  Divider(),
                  RichText(
                    text: TextSpan(
                      text: "My Works",
                      style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                      )
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 60,
              child: MyLists()
            )
          ],
        ),
      ),
    );
  }
}


